import random
from const import NUMBER_OF_AUTOKARY as NUMBER_OF_AUTOKARY


def generate_autokary():
    fo = open("data/autokary.sql", "w+", encoding='utf-8')
    for i in range(1, NUMBER_OF_AUTOKARY + 1):
        print("INSERT INTO rautokary (id_autokaru, liczba_miejsc) VALUES (" + str(i) + ", " + str(random.randint(20, 100)) + ");\n/", file=fo)
        print("COMMIT;\n/", file=fo)
    fo.close()
