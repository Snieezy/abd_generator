import random
from const import NUMBER_OF_PEOPLE as NUMBER_OF_PEOPLE
from const import NUMBER_OF_TOURS as NUMBER_OF_TOURS


def generate_participants():
    fo = open("data/participants.sql", "w+", encoding='utf-8')
    nr_umowy = 11111111
    for i in range(1, NUMBER_OF_PEOPLE + 1):
        if i % 10 != 0:
            print("INSERT INTO ruczestnicy (id_uczestnika, znizka, wiek, nr_umowy, id_osoby, id_turnusu) VALUES (" + str(
                i) + ", " + str(random.randint(1, 50)) + ", " + str(random.randint(20, 85)) + ", " + str(
                nr_umowy) + ", " + str(i) + ", " + str(random.randint(1, NUMBER_OF_TOURS)) + ");\n/", file=fo)
        nr_umowy += 1
        print("COMMIT;\n/", file=fo)
    fo.close()
