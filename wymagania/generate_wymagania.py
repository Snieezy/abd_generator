import random
from const import NUMBER_OF_PEOPLE as NUMBER_OF_PEOPLE


def generate_wymagania():
    fo = open("data/wymagania.sql", "w+", encoding='utf-8')
    opisy = {"jedzenie": ["Potrawy wegańskie", "Potrawy wegetariańskie", "Potrawy bezglutenowe"],
             "hotel": ["Z basenem", "Z siłownią", "Z placem zabaw", "Akceptacja zwierząt domowych"],
             "atrakcje": ["Muzea", "Pomniki", "Zabytki XX w.", "Galerie sztuki"]}
    czego_dotyczy = ["jedzenie", "hotel", "atrakcje"]
    for i in range(1, NUMBER_OF_PEOPLE + 1):
        if i % 10 != 0 and random.random() < 0.8:
            dotyczy = random.choice(czego_dotyczy)
            print("INSERT INTO rwymagania (id_wymagania, opis, czego_dotyczy, id_uczestnika) VALUES (" + str(
                i) + ", '" + random.choice(opisy[dotyczy]) + "', '" + dotyczy + "'," + str(i) + ");\n/", file=fo)
    print("COMMIT;\n/", file=fo)
    fo.close()


generate_wymagania()
