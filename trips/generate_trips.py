'''
insert into RWycieczki
(
  id_wycieczki,
  kraj,
  miasto,
  czas
)
values
(
  1,
  'Panama',
  'Gdynia',
  '12'
)
'''

import random
import datetime
from const import NUMBER_OF_TRIPS as NUMBER_OF_TRIPS


def read_countries_with_cities():
    countries_with_cities = []
    with open("input/countrieswithcities.txt", "r", encoding='utf-8') as f:
        for line in f:
            countries_with_cities.append(line.rstrip())
    return countries_with_cities


def generate_trips():
    fo = open("data/trips.sql", "w+", encoding='utf-8')
    countries_with_cities = read_countries_with_cities()
    for i in range(1, NUMBER_OF_TRIPS + 1):
        if i < NUMBER_OF_TRIPS/5:
            print("INSERT INTO RWycieczki (id_wycieczki, kraj, miasto, czas) VALUES (" + str(i) + ", '" + random.choice(countries_with_cities).replace(",", "', '") + "', '" + str(2) + "');\n/", file=fo)
        elif NUMBER_OF_TRIPS/5 < i < 2 * NUMBER_OF_TRIPS/5:
            print("INSERT INTO RWycieczki (id_wycieczki, kraj, miasto, czas) VALUES (" + str(i) + ", '" + random.choice(countries_with_cities).replace(",", "', '")  + "', '" + str(20) + "');\n/", file=fo)
        elif 2 * NUMBER_OF_TRIPS/5 < i < 3 * NUMBER_OF_TRIPS/5:
            print("INSERT INTO RWycieczki (id_wycieczki, kraj, miasto, czas) VALUES (" + str(i) + ", '" + random.choice(countries_with_cities).replace(",", "', '")  + "', '" + str(30) + "');\n/", file=fo)
        elif 3 * NUMBER_OF_TRIPS/5 < i < 4 * NUMBER_OF_TRIPS/5:
            print("INSERT INTO RWycieczki (id_wycieczki, kraj, miasto, czas) VALUES (" + str(i) + ", '" + random.choice(countries_with_cities).replace(",", "', '")  + "', '" + str(40) + "');\n/", file=fo)
        else:
            print("INSERT INTO RWycieczki (id_wycieczki, kraj, miasto, czas) VALUES (" + str(i) + ", '" + random.choice(countries_with_cities).replace(",", "', '")  + "', '" + str(50) + "');\n/", file=fo)
        print("COMMIT;\n/", file=fo)
    fo.close()


def generate_tours():
    generate_trips()
    fo = open("data/tours.sql", "w+", encoding='utf-8')
    for i in range(1, 4 * NUMBER_OF_TRIPS + 1):
        start_date = datetime.date.today().replace(day=1, month=1).toordinal()
        end_date = datetime.date.today().toordinal()
        random_day = datetime.date.fromordinal(random.randint(start_date, end_date))
        print("INSERT INTO rturnusy (id_turnusu, od, do, id_wycieczki) VALUES (" + str(i) + ", '" + str(random_day) + "', '" + str(random_day + datetime.timedelta(days=2)) + "', " + str(random.randint(1, NUMBER_OF_TRIPS)) + ");\n/", file=fo)
        print("COMMIT;\n/", file=fo)
