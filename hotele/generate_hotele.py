import datetime
import random
from const import NUMBER_OF_HOTELE as NUMBER_OF_HOTELE
from faker import Factory


def generate_hotele():
    fo = open("data/hotele.sql", "w+", encoding='utf-8')
    fake = Factory.create()
    for i in range(1, NUMBER_OF_HOTELE + 1):
        print("INSERT INTO rhotelpobytowy (id_hotelu, nazwa, adres, liczba_gwiazdek) VALUES (" + str(
            i) + ", '" + fake.state() + "', '" + fake.address() + "', " + str(random.randint(1, 5)) + ");\n/", file=fo)
        print("COMMIT;\n/", file=fo)
    fo.close()
