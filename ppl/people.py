'''
insert into ROsoby
(
  ID_OSOBY,
  IMIE,
  NAZWISKO,
  ADRES,
  NR_PASZPORTU
)
values
(
  15,
  'Hildegarda',
  'Górczyńska',
  'Daliowa',
  'aaaa1124'
)
'''

import random
from const import NUMBER_OF_PEOPLE as NUMBER_OF_PEOPLE
from faker import Factory


def generate_people():
    fo = open("data/people.sql", "w+", encoding='utf-8')
    passport_number = 11111111
    fake = Factory.create()
    for i in range(NUMBER_OF_PEOPLE + 1):
        print("INSERT INTO ROsoby (ID_OSOBY, IMIE, NAZWISKO, ADRES, NR_PASZPORTU) VALUES (" + str(i) + ", '" + fake.first_name() + "', '" + fake.last_name() + "', '" + fake.address() + "', " + str(passport_number) + ");\n/", file=fo)
        print("COMMIT;\n/", file=fo)
        passport_number += 1
    fo.close()
