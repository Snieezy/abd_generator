import datetime
import random
from const import NUMBER_OF_PEOPLE as NUMBER_OF_PEOPLE
from const import NUMBER_OF_JEZYKI as NUMBER_OF_JEZYKI


def generate_piloci_jezyki():
    fo = open("data/piloci_jezyki.sql", "w+", encoding='utf-8')
    for i in range(10, NUMBER_OF_PEOPLE + 1, 10):
        random_number1 = random.randint(1, NUMBER_OF_JEZYKI)
        random_number2 = random.randint(1, NUMBER_OF_JEZYKI)
        random_number3 = random.randint(1, NUMBER_OF_JEZYKI)
        print("INSERT INTO rpiloci_rjezyki (id_osoby, id_jezyka) VALUES (" + str(i) + ", " + str(random.randint(1, NUMBER_OF_JEZYKI)) + ");\n/", file=fo)
        if random.random() < 0.9 and random_number1 != random_number2:
            print("INSERT INTO rpiloci_rjezyki (id_osoby, id_jezyka) VALUES (" + str(i) + ", " + str(random.randint(1, NUMBER_OF_JEZYKI)) + ");\n/", file=fo)
        if random.random() < 0.4 and random_number3 != random_number1 and random_number3 != random_number2:
            print("INSERT INTO rpiloci_rjezyki (id_osoby, id_jezyka) VALUES (" + str(i) + ", " + str(random.randint(1, NUMBER_OF_JEZYKI)) + ");\n/", file=fo)
        print("COMMIT;\n/", file=fo)
    fo.close()
