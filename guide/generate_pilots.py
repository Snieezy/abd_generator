import datetime
import random
from const import NUMBER_OF_PEOPLE as NUMBER_OF_PEOPLE


def generate_pilots():
    fo = open("data/pilot.sql", "w+", encoding='utf-8')
    for i in range(10, NUMBER_OF_PEOPLE + 1, 10):
        start_date = datetime.date(2015, 1, 1).toordinal()
        end_date = datetime.date(2015, 12, 31).toordinal()
        random_day = datetime.date.fromordinal(random.randint(start_date, end_date))
        print("INSERT INTO rpiloci (staz, zatrudniony_od, id_osoby) VALUES (" + str(random.randint(1, 20)) + ", '" + str(random_day) + "', " + str(i) + ");\n/", file=fo)
        print("COMMIT;\n/", file=fo)
    fo.close()
