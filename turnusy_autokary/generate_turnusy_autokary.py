import random
from const import NUMBER_OF_AUTOKARY as NUMBER_OF_AUTOKARY
from const import NUMBER_OF_TOURS as NUMBER_OF_TOURS


def generate_turnusy_autokary():
    fo = open("data/turnusy_autokary.sql", "w+", encoding='utf-8')
    for i in range(1, NUMBER_OF_AUTOKARY + 1):
        random_number1 = random.randint(1, NUMBER_OF_TOURS)
        random_number2 = random.randint(1, NUMBER_OF_TOURS)
        random_number3 = random.randint(1, NUMBER_OF_TOURS)
        print("INSERT INTO rturnusy_rautokary (id_autokaru, id_turnusu) VALUES (" + str(i) + ", " + str(random_number1) + ");\n/", file=fo)
        if random.random() < 0.2 and random_number2 != random_number1:
            print("INSERT INTO rturnusy_rautokary (id_autokaru, id_turnusu) VALUES (" + str(i) + ", " + str(random_number2) + ");\n/", file=fo)
        if random.random() < 0.1 and random_number3 != random_number1 and random_number3 != random_number2:
            print("INSERT INTO rturnusy_rautokary(id_autokaru, id_turnusu) VALUES (" + str(i) + ", " + str(random_number3) + ");\n/", file=fo)
        print("COMMIT;\n/", file=fo)
    fo.close()
