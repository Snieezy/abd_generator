import datetime
import random
from const import NUMBER_OF_ETAPY as NUMBER_OF_ETAPY
from const import NUMBER_OF_TRIPS as NUMBER_OF_TRIPS


def read_countries_with_cities():
    countries_with_cities = []
    with open("input/countrieswithcities.txt", "r", encoding='utf-8') as f:
        for line in f:
            countries_with_cities.append(line.rstrip())
    return countries_with_cities


def generate_etapy():
    fo = open("data/etapy.sql", "w+", encoding='utf-8')
    countries_with_cities = read_countries_with_cities()
    for i in range(1, NUMBER_OF_ETAPY + 1):
        if i % 3 == 0:
            print("INSERT INTO retapy (id_etapu, miejsce, obiekt, czas_pobytu, id_wycieczki) VALUES (" + str(i) + ", '" + random.choice(countries_with_cities) + "', '" + "muzeum" + "', '" + str(random.randint(1, 10)) + "', " + str(random.randint(1, NUMBER_OF_TRIPS)) + ");\n/", file=fo)
        elif i % 3 == 1:
            print("INSERT INTO retapy (id_etapu, miejsce, obiekt, czas_pobytu, id_wycieczki) VALUES (" + str(i) + ", '" + random.choice(countries_with_cities) + "', '" + "pomnik" + "', '" + str(random.randint(1, 10)) + "', " + str(random.randint(1, NUMBER_OF_TRIPS)) + ");\n/", file=fo)
        else:
            print("INSERT INTO retapy (id_etapu, miejsce, obiekt, czas_pobytu, id_wycieczki) VALUES (" + str(i) + ", '" + random.choice(countries_with_cities) + "', '" + "potato" + "', '" + str(random.randint(1, 10)) + "', " + str(random.randint(1, NUMBER_OF_TRIPS)) + ");\n/", file=fo)
    print("COMMIT;\n/", file=fo)
    fo.close()
