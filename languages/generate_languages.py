def read_languages():
    languages = []
    with open("input/languages.txt", "r", encoding='utf-8') as f:
        for line in f:
            languages.append(line.rstrip())
    return languages


def generate_languages():
    fo = open("data/languages.sql", "w+", encoding='utf-8')
    languages = read_languages()
    lang_id = 1
    print("INSERT ALL", file=fo)
    for lang in languages:
        print("INTO rjezyki (id_jezyka, nazwa) VALUES (" + str(lang_id) + ", '" + lang + "')", file=fo)
        lang_id += 1
    print("SELECT 1 FROM DUAL;", file=fo)
    fo.close()
