import random
from const import NUMBER_OF_HOTELE as NUMBER_OF_HOTELE
from const import NUMBER_OF_TOURS as NUMBER_OF_TOURS


def generate_turnusy_hotele():
    fo = open("data/turnusy_hotele.sql", "w+", encoding='utf-8')
    for i in range(1, NUMBER_OF_HOTELE + 1):
        random_number1 = random.randint(1, NUMBER_OF_TOURS)
        random_number2 = random.randint(1, NUMBER_OF_TOURS)
        random_number3 = random.randint(1, NUMBER_OF_TOURS)
        print("INSERT INTO rturnusy_rhotele (id_hotelu, id_turnusu) VALUES (" + str(i) + ", " + str(random_number1) + ");\n/", file=fo)
        if random.random() < 0.2 and random_number1 != random_number2:
            print("INSERT INTO rturnusy_rhotele (id_hotelu, id_turnusu) VALUES (" + str(i) + ", " + str(random_number2) + ");\n/", file=fo)
        if random.random() < 0.1 and random_number1 != random_number3 and random_number2 != random_number3:
            print("INSERT INTO rturnusy_rhotele (id_hotelu, id_turnusu) VALUES (" + str(i) + ", " + str(random_number3) + ");\n/", file=fo)
        print("COMMIT;\n/", file=fo)
    fo.close()
