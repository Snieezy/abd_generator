from const import NUMBER_OF_ATRAKCJE as NUMBER_OF_ATRAKCJE


def generate_atrakcje():
    fo = open("data/atrakcje.sql", "w+", encoding='utf-8')
    for i in range(1, NUMBER_OF_ATRAKCJE + 1):
        if i % 3 == 0:
            print("INSERT INTO ratrakcje (id_atrakcji, opis) VALUES (" + str(i) + ", '" + "muzeum" + "');\n/", file=fo)
        elif i % 3 == 1:
            print("INSERT INTO ratrakcje (id_atrakcji, opis) VALUES (" + str(i) + ", '" + "pole bitwy" + "');\n/", file=fo)
        else:
            print("INSERT INTO ratrakcje (id_atrakcji, opis) VALUES (" + str(i) + ", '" + "pomnik" + "');\n/", file=fo)
        print("COMMIT;\n/", file=fo)
    fo.close()

generate_atrakcje()
