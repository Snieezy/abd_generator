from atrakcje import generate_atrakcje
from autokary import generate_autokary
from etapy import generate_etapy
from guide import generate_pilots
from hotele import generate_hotele
from languages import generate_languages
from participant import generate_participants
from piloci_jezyki import generate_piloci_jezyki
from ppl import people
from trips import generate_trips
from turnusy_autokary import generate_turnusy_autokary
from turnusy_hotele import generate_turnusy_hotele
from wymagania import generate_wymagania

print("Atrakcje...")
generate_atrakcje.generate_atrakcje()

print("Autokary...")
generate_autokary.generate_autokary()

print("Etapy...")
generate_etapy.generate_etapy()

print("Piloci...")
generate_pilots.generate_pilots()

print("Hotele...")
generate_hotele.generate_hotele()

print("Języki...")
generate_languages.generate_languages()

print("Uczestnicy...")
generate_participants.generate_participants()

print("Piloci-języki...")
generate_piloci_jezyki.generate_piloci_jezyki()

print("Osoby...")
people.generate_people()

print("Wycieczki i turnusy...")
generate_trips.generate_tours()

print("Turnusy-autokary...")
generate_turnusy_autokary.generate_turnusy_autokary()

print("Turnusy-hotele...")
generate_turnusy_hotele.generate_turnusy_hotele()

print("Wymagania...")
generate_wymagania.generate_wymagania()
